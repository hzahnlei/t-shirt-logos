# T-Shirt Logos

Logos for printing T-shirts (or whatever).

Holger Zahnleiter<br>
2021-03-20

## About The Files

I am using Affinity Designer for creating vector drawings.
In addition I am providing SVG exports since Affinity is available on mac OS only.

*  The original/source Affinity drawings are found under `source`.
*  The SVG exports are found under `svg_export`.
*  `avatar` contains the GitLab project avatar picture.

## Legal Note

All logos found herein have been drawn by me for non-commercial purposes, for example printing nerd T-shirts for myself.

I did not invent or design the logos.

All logos and trademarks found herein are the property of their respective owners.
